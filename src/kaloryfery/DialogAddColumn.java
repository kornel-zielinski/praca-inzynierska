package kaloryfery;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.SpringLayout;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.JTextField;

public class DialogAddColumn extends JDialog implements Runnable{

	private static final long serialVersionUID = 1L;
	JComboBox<DBHandler.Type> comboBoxTypes;
	private JTextField textFieldColumnName;
	private String tableName;
	private JButton buttonOK;
	private JButton buttonCancel;
	
	private static final String CANCEL = "cancel";
	private static final String OK = "OK";
	
	public static void main(String[] args) {
		try {
			EventQueue.invokeLater(new DialogAddColumn("ayy"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		setVisible(true);
	}
	
	public void showDialog() {
		setVisible(true);
	}	


	public DialogAddColumn(String tableName) {
		setBounds(100, 100, 250, 200);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		getContentPane().setLayout(springLayout);

		this.tableName = tableName;
		
		buttonOK = new JButton("OK");
		springLayout.putConstraint(SpringLayout.SOUTH, buttonOK, -16, SpringLayout.SOUTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, buttonOK, -16, SpringLayout.EAST, getContentPane());
		buttonOK.setEnabled(false);
		buttonOK.addActionListener(new ButtonActionListener());
		getContentPane().add(buttonOK);
		
		buttonCancel = new JButton("Cancel");
		springLayout.putConstraint(SpringLayout.SOUTH, buttonCancel, -16, SpringLayout.SOUTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, buttonCancel, -8, SpringLayout.WEST, buttonOK);
		buttonCancel.addActionListener(new ButtonActionListener());
		getContentPane().add(buttonCancel);
		
		
		textFieldColumnName = new JTextField();
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, textFieldColumnName, 0, SpringLayout.HORIZONTAL_CENTER, getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, textFieldColumnName, 16, SpringLayout.NORTH, getContentPane());
		textFieldColumnName.setColumns(10);
		textFieldColumnName.getDocument().addDocumentListener(new TextColumnNameListener());
		getContentPane().add(textFieldColumnName);
		
		comboBoxTypes = new JComboBox<>(DBHandler.Type.values());
		springLayout.putConstraint(SpringLayout.NORTH, comboBoxTypes, 8, SpringLayout.SOUTH, textFieldColumnName);
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, comboBoxTypes, 0, SpringLayout.HORIZONTAL_CENTER, getContentPane());
		getContentPane().add(comboBoxTypes);
		
	}
	
	private class ButtonActionListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			switch(e.getActionCommand()) {
			case OK:
				String columnName = textFieldColumnName.getText().trim();
				DBHandler.Type columnType = (DBHandler.Type) comboBoxTypes.getSelectedItem();
				try {
					DBHandler handler = DBHandler.getHandler(tableName);
					handler.addColumn(columnName, columnType.toString());
				}catch(Exception ex){
					ex.printStackTrace();
					return;
				}
				break;
			case CANCEL:
				break;
			}
			dispose();			
		}
	}
	
	private class TextColumnNameListener implements DocumentListener{

		@Override
		public void removeUpdate(DocumentEvent e) {
			if(textFieldColumnName.getText().length() == 0) {
				buttonOK.setEnabled(false);
			}
		}
		
		@Override
		public void insertUpdate(DocumentEvent e) {
			if(textFieldColumnName.getText().trim().length() != 0) {
				buttonOK.setEnabled(true);
			}
		}
		
		@Override
		public void changedUpdate(DocumentEvent e) {
		}
	}
}
