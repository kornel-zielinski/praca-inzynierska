package kaloryfery;

import java.util.LinkedList;
import java.util.List;

import javax.swing.AbstractListModel;

public class DeviceListModel extends AbstractListModel<String> {
	private static final long serialVersionUID = 1L;
	private List<String> tables;
	
	public DeviceListModel() {
		try{ 
			tables = DBHandler.getAllTables();
			if(tables == null)
				tables = new LinkedList<String>();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public int getSize() { 
		return tables.size(); 
		}
	
	@Override
    public String getElementAt(int index) { 
    	return tables.get(index);
    }
    
    public void refresh() {
    	try{ 
    		tables = DBHandler.getAllTables();
    	}
		catch(Exception e){
			e.printStackTrace();
		}
    }
}
