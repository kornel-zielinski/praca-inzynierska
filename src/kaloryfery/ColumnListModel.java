package kaloryfery;

import java.util.LinkedList;
import java.util.List;

import javax.swing.AbstractListModel;

public class ColumnListModel extends AbstractListModel<String> {
	private static final long serialVersionUID = 1L;
	private List<String> columns;
	private List<String> types;
	private String deviceName;
	
	public ColumnListModel() {
		try{ 
			//tables = DBHandler.getAllTables();
			columns = new LinkedList<String>();
			types = new LinkedList<String>();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public int getSize() { 
		return columns.size(); 
		}
	
	@Override
    public String getElementAt(int index) { 
    	return columns.get(index) + " (" + types.get(index) + ")";
    }
    
    public void refresh() {
    	try {
    		DBHandler handler = DBHandler.getHandler(deviceName);
    		columns = handler.getColumnNames();
    		types = new LinkedList<String>();
    		for(String column: columns) {
    			types.add(handler.getTypeOfColumn(column).toString());
    		}
		} catch (Exception e) {
			types = new LinkedList<String>();
			columns = new LinkedList<String>();
		}
    }
    
    public void setDeviceName(String name) {
    	try {
    		DBHandler handler = DBHandler.getHandler(name);
    		deviceName = name;
    		columns = handler.getColumnNames();
    		types = new LinkedList<String>();
    		for(String column: columns) {
    			types.add(handler.getTypeOfColumn(column).toString());
    		}
		} catch (Exception e) {
			columns = new LinkedList<String>();
    		types = new LinkedList<String>();
		}
		
    }
}
