package kaloryfery;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
//import java.util.Random;

public class DBHandler {
	
	public enum Type{
		INTEGER, DOUBLE, BOOLEAN
	}
	
	//private static final String PROTOCOL = "jdbc:derby://localhost:1527/";
	private static final String PROTOCOL = "jdbc:derby:";
	private static final String DATABASE_NAME = "db3000";
	private static HashMap<String, DBHandler> tableMap = null;
	private static Connection connection; 
	
	public String name;
	private ArrayList<String> columnNames;
	private ArrayList<Type> columnTypes;
	private PreparedStatement insertStatement;
	private PreparedStatement selectAll;
	private PreparedStatement selectAllInRange;
	private Statement statement;
	
	
	private static synchronized void init() throws SQLException {
		if(tableMap == null)
			tableMap = new HashMap<String, DBHandler>();
		if(connection == null)
			connection = DriverManager.getConnection(PROTOCOL + DATABASE_NAME + ";create=true");
	}
	
	public static synchronized DBHandler getHandler(String tableName) throws SQLException {
		init();
		DBHandler handler = tableMap.get(tableName.toUpperCase());
		if(handler == null) {
			DatabaseMetaData metadata = connection.getMetaData();
			ResultSet resultSet = metadata.getColumns(null, "APP", tableName.toUpperCase(), null);

			if(resultSet.next()) {
				handler = new DBHandler();
				handler.name = tableName.toUpperCase();
				tableMap.put(handler.name, handler);
				handler.columnNames = new ArrayList<String>();
				handler.columnTypes = new ArrayList<Type>();
				resultSet.next();                //omijamy id i dat�
				while(resultSet.next()){
					handler.columnNames.add(resultSet.getString(4).toUpperCase());
					handler.columnTypes.add(Type.valueOf(resultSet.getString(6).toUpperCase()));
				} 

				handler.prepareStatements();
			}
		}
		return handler;	
	}
	
	public static synchronized DBHandler createTable(String tableName, String[] names, String[] types) throws SQLException {
		init();
			
		//Czy ju� istnieje?
		DBHandler handler = getHandler(tableName);
		if(handler != null) {
			throw(new SQLException("Tabela o danej nazwie ju� istnieje"));
		} else {
			handler = new DBHandler();
			handler.name = tableName.toUpperCase();
			tableMap.put(handler.name, handler);
			
			int size = 0;
			if(names != null && types != null) {	
				size = Math.min(names.length, types.length);
			}
			handler.columnNames = new ArrayList<String>(size);
			handler.columnTypes = new ArrayList<Type>  (size);
			
			//tworzymy tabel� w bazie danych
			String createStatement = "CREATE TABLE " + handler.name + "(id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, data TIMESTAMP";
			for(int i = 0; i<size; ++i) {
				handler.columnTypes.add(Type.valueOf(types[i].toUpperCase()));
				handler.columnNames.add(names[i].toUpperCase());
				createStatement = createStatement + ", " + names[i] + " " + types[i];
			}
			createStatement = createStatement + ")";
			
			//System.out.println(createStatement);
			Statement statement = connection.createStatement();
			statement.execute(createStatement);
			statement.close();
			
			//inicjalizujemy obiekty PreparedStatement w�a�ciwe dla tabeli
			handler.prepareStatements();
			return handler;	
		}	
	}
	
	public static List<String> getAllTables() throws SQLException{
		init();
		LinkedList<String> tableList = new LinkedList<>();
		DatabaseMetaData metadata = connection.getMetaData();
		ResultSet resultSet = metadata.getTables(null, "APP", null, null);
		while(resultSet.next()) {
			tableList.add(resultSet.getString(3));
		}
		return tableList;
	}
	
	//wywo�ywa� po init()!
	private void prepareStatements() throws SQLException {
		String columns = "";
		for(String name : columnNames) {
			columns = columns + ", " + name;
		}

		selectAll = connection.prepareStatement("SELECT id, data " + columns + " FROM " + name + " ORDER BY data");
		selectAllInRange = connection.prepareStatement("SELECT id, data " + columns + " FROM " + name + " WHERE data BETWEEN ? AND ? ORDER BY data");
		
		String columnNamesSQL = "data";
		String values = "?";
		for(String column : columnNames) {
			columnNamesSQL = columnNamesSQL + ", " + column; 
			values = values + ", ?";
		}
		insertStatement = connection.prepareStatement("INSERT INTO "+name+"("+columnNamesSQL+") VALUES("+values+")");
		statement = connection.createStatement();
	}
	
	private DBHandler() {
		super();
	}
	
	public synchronized void addColumn(String column, String type) throws SQLException {
		for(String existingColumn : columnNames) {
			if(existingColumn.toUpperCase().equals(column.toUpperCase()))
				throw(new SQLException("Kolumna o podanej nazwie ju� istnieje"));
		}
		Type actualType = null;
		try{
			actualType = Type.valueOf(type.toUpperCase());
		}catch (Exception e) {
			throw(new SQLException("Niepoprawny typ"));
		}
		//Statement statement = connection.createStatement();
		statement.execute("ALTER TABLE " + name + " ADD " + column + " " + actualType);
		//statement.close();
		
		columnNames.add(column.toUpperCase());
		columnTypes.add(Type.valueOf(type.toUpperCase()));
		prepareStatements();
	}
	
	public synchronized void dropColumn(String column) throws SQLException {
		Boolean found = false;
		for(String existingColumn : columnNames) {
			if(existingColumn.toUpperCase().equals(column.toUpperCase())) {
				found = true;
				break;
			}
		}
		if (!found)
			throw(new SQLException("Kolumna o podanej nazwie nie istnieje"));
		//Statement statement = connection.createStatement();
		statement.execute("ALTER TABLE " + name + " DROP COLUMN " + column);
		//statement.close();
		
		int i = columnNames.indexOf(column.toUpperCase());
		columnNames.remove(i);
		columnTypes.remove(i);
		prepareStatements();
	}
	
	public ResultSet select(){
		ResultSet result = null;
		try {
			result =  selectAll.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public ResultSet select(Date from, Date to){
		ResultSet result = null;
		try {
			selectAllInRange.setDate(1, from);
			selectAllInRange.setDate(2, to);
			result =  selectAllInRange.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public ResultSet selectColumn(String column){
		ResultSet result = null;
		try {
			result = statement.executeQuery("SELECT data, "+column+" FROM "+name+" ORDER BY data");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public ResultSet selectColumnInRange(String column, Timestamp from, Timestamp to){
		ResultSet result = null;
		try {
			result = statement.executeQuery("SELECT data, "+column+" FROM "+name+" WHERE data BETWEEN \'"+from+"\' AND \'"+to+"\' ORDER BY data");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public <T> void selectColumnAsArrays(String column, ArrayList<Timestamp> dates, ArrayList<T> values) {
		ResultSet resultSet = null;
		resultSet = selectColumn(column);
		if(resultSet != null) {
			try {
				while(resultSet.next()){
					Timestamp date = resultSet.getTimestamp(1);
					dates.add(date);
					Object value = resultSet.getObject(2);
					values.add((T) value);
				}
				resultSet.close();
			}catch(Exception e) {e.printStackTrace();}
		
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> void selectColumnAsArrays(String column, ArrayList<Timestamp> dates, ArrayList<T> values, Timestamp from, Timestamp to) {
		ResultSet resultSet = null;
		resultSet = selectColumnInRange(column, from, to);
		if(resultSet != null) {
			try {
				while(resultSet.next()){
					Timestamp date = resultSet.getTimestamp(1);
					dates.add(date);
					Object value = resultSet.getObject(2);
					values.add((T) value);
				}
				resultSet.close();
			}catch(Exception e) {e.printStackTrace();}
		
		}
	}
	
	
	public synchronized void insert(Timestamp date, Object[] values) throws SQLException {
		insertStatement.setTimestamp(1, date);
		for(int i = 0; i < values.length; ++i) {
			switch(columnTypes.get(i)) {
			case INTEGER:
				insertStatement.setInt(i+2, (Integer) values[i]);
				break;
			case DOUBLE:
				insertStatement.setDouble(i+2, (Double) values[i]);
				break;
			case BOOLEAN:
				insertStatement.setBoolean(i+2, (Boolean) values[i]);
				break;
			default:
				throw(new SQLException("Unsupported type"));	
			}
		}
		insertStatement.executeUpdate();
	}
	
	public Type getTypeOfColumn(String name) {
		int index = columnNames.indexOf(name.toUpperCase());
		if(index != -1)
			return columnTypes.get(index);
		else return null;
	}
	
	public List<String> getColumnNames() {
		return new ArrayList<String>(columnNames);
	}
	
	public int getColumnCount() {
		return columnNames.size();
	}
	
	public void drop() throws SQLException {
		statement.execute("DROP TABLE " + name);
		statement.close();	
		tableMap.remove(name);
	}
	
	public static synchronized void close() throws SQLException {
		
		for(Map.Entry<String, DBHandler> entry : tableMap.entrySet()) {
			DBHandler handler = entry.getValue();
			if(!handler.statement.isClosed())
				handler.statement.close();
			if(!handler.insertStatement.isClosed())
				handler.insertStatement.close();
			if(!handler.selectAll.isClosed())
				handler.selectAll.close();
			if(!handler.selectAllInRange.isClosed())
				handler.selectAllInRange.close();
		}
		if(tableMap != null)
			tableMap = null;
		if(connection != null)
			connection.close();
	}


	public static void main(String[] args) throws SQLException{
		//createTable("lmao", new String[] {"p1",  "p2"}, new String[] {"INTEGER", "INTEGER"});
		init();
		createTable("asdf", null, null);
		System.out.println(getAllTables());
/*
		//Random random = new Random();
		DBHandler handler = getHandler("ayy");

		for(String col : handler.columnNames)
			System.out.println(col);
		System.out.println(handler.getTypeOfColumn("pomiar2"));
		//handler.insert(new Timestamp(System.currentTimeMillis()), new Object[] {random.nextInt(30), random.nextDouble(), random.nextBoolean(), random.nextInt(30)});
		
		ResultSet resultSet = handler.selectColumn("pomiar3");
		if(resultSet != null) {
			while(resultSet.next()){
				Timestamp date = resultSet.getTimestamp(1);
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				System.out.format("%s ", format.format(date));
				Object temp = resultSet.getObject(2);  
				System.out.append(temp + " ");
				System.out.format("%n");
			}
		}
		resultSet.close();
		System.out.format("%n");
		ArrayList<Timestamp> dates = new ArrayList<Timestamp>();
		ArrayList<Integer> values = new ArrayList<Integer>();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		handler.selectColumnAsArrays("pomiar1", dates, values);
		for(int i = 0; i<dates.size(); ++i) {
			System.out.format("%s %d %n", format.format(dates.get(i)), values.get(i));
		}
		
		System.out.println(handler.getColumnNames());	
		close();
		*/
	}
	

}
