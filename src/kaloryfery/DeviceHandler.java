package kaloryfery;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Scanner;

public class DeviceHandler extends Thread{
	protected ServerSocket serverSocket;
	
	public DeviceHandler(int port) {
		try {
			serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	@Override
	public void run(){
		try {
			while(true)
				new Thread(new Handler(serverSocket.accept())).start();
		} catch (SocketException e) {}
		  catch (Exception e) {
			e.printStackTrace();
		};
	}

	public static void main(String[] args) {
		DeviceHandler deviceHandler = new DeviceHandler(1234);
		deviceHandler.start();
		System.out.println("w�tek idzie dalej");
		try {
			Thread.sleep(1000*10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		deviceHandler.close();
	}
	
	protected class Handler implements Runnable {
		protected Socket socket;
		
		public Handler(Socket socket) {
			this.socket = socket;
		}

		@Override
		public void run() {
			try {
				System.out.println(new Date() + " connection received from "+socket.getInetAddress());
				Scanner scanner = new Scanner(socket.getInputStream());
				scanner.useLocale(Locale.US);
				//System.out.println(scanner.nextLine());
	            LinkedList<Object> values = new LinkedList<>();
	            DBHandler handler = DBHandler.getHandler(scanner.next());
	            Timestamp date = new Timestamp(scanner.nextLong()*1000);
	            for(String column: handler.getColumnNames()) {
	            	switch(handler.getTypeOfColumn(column)) {
	            	case INTEGER:
	            		values.add(scanner.nextInt());
	            		break;
	            	case BOOLEAN:
	            		values.add(scanner.nextInt() == 1);
	            		break;
	            	case DOUBLE:
	            		values.add(scanner.nextDouble());
	            		break;
	            	}
	            }
	            
	            scanner.close();
	            handler.insert(date, values.toArray());
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
		}

	}

	public void close(){
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
