package kaloryfery;

import java.awt.Button;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class MainWindow implements Runnable {
	
	private JFrame frame;
	private JList<String> listDevices;
	private JList<String> listColumns;
	private JTextField textPort;
	
	private static final String DEV_ADD = "add device";
	private static final String DEV_DEL = "delete device";
	private static final String DEV_REF = "refresh list of devices";
	private static final String COL_ADD = "add column";	
	private static final String COL_DEL = "delete column";
	private static final String COL_REF = "refresh list of columns";
	private static final String LISTEN  = "toggle receiving transmissions";
	private static final String GRAPH   = "open graph window";
	
	private int port = 1234;
	private DeviceHandler deviceHandler;
	
	public static void main(String[] args) {
		MainWindow window = new MainWindow();
		try {
			if(args.length > 0)
				window.port = Integer.valueOf(args[0]);
		}catch(Exception e) {}
		EventQueue.invokeLater(window);
	}

	@Override
	public void run() {
		frame.setVisible(true);
	}
	
	public MainWindow() {
		frame = new JFrame();
		frame.setMinimumSize(new Dimension(500, 350));
		frame.setBounds(100, 100, 704, 506);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Super serwer trzy miliony");
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		listDevices = new JList<String>(new DeviceListModel());
		listDevices.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listDevices.addListSelectionListener(new DeviceListSelectionListener());
		
		JScrollPane scrollPaneDevices = new JScrollPane(listDevices);
		springLayout.putConstraint(SpringLayout.NORTH, scrollPaneDevices, 16, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, scrollPaneDevices, 16, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, scrollPaneDevices, -50, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, scrollPaneDevices, -108, SpringLayout.HORIZONTAL_CENTER, frame.getContentPane());
		frame.getContentPane().add(scrollPaneDevices);
		
		listColumns = new JList<String>(new ColumnListModel());
		listColumns.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane scrollPaneColumns = new JScrollPane(listColumns);
		springLayout.putConstraint(SpringLayout.NORTH, scrollPaneColumns, 16, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, scrollPaneColumns, -92, SpringLayout.HORIZONTAL_CENTER, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, scrollPaneColumns, -50, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, scrollPaneColumns, -16, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(scrollPaneColumns);
		
		ButtonActionListener buttonActionListener = new ButtonActionListener();
		
		Button buttonRefreshDeviceList = new Button("Od�wie�");
		buttonRefreshDeviceList.setSize(new Dimension(200, 20));
		springLayout.putConstraint(SpringLayout.WEST, buttonRefreshDeviceList, 16, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, buttonRefreshDeviceList, -16, SpringLayout.SOUTH, frame.getContentPane());
		buttonRefreshDeviceList.addActionListener(buttonActionListener);
		buttonRefreshDeviceList.setActionCommand(DEV_REF);
		frame.getContentPane().add(buttonRefreshDeviceList);
		
		Button buttonAddDevice = new Button("+");
		springLayout.putConstraint(SpringLayout.WEST, buttonAddDevice, 8, SpringLayout.EAST, buttonRefreshDeviceList);
		springLayout.putConstraint(SpringLayout.SOUTH, buttonAddDevice, -16, SpringLayout.SOUTH, frame.getContentPane());
		buttonAddDevice.setActionCommand(DEV_ADD);
		buttonAddDevice.addActionListener(buttonActionListener);
		frame.getContentPane().add(buttonAddDevice);
		
		Button buttonDeleteDevice = new Button("-");
		springLayout.putConstraint(SpringLayout.WEST, buttonDeleteDevice, 8, SpringLayout.EAST, buttonAddDevice);
		springLayout.putConstraint(SpringLayout.SOUTH, buttonDeleteDevice, -16, SpringLayout.SOUTH, frame.getContentPane());
		buttonDeleteDevice.addActionListener(buttonActionListener);
		buttonDeleteDevice.setActionCommand(DEV_DEL);
		frame.getContentPane().add(buttonDeleteDevice);
		
		Button buttonRefreshColumnList = new Button("Od�wie�");
		buttonRefreshColumnList.setSize(new Dimension(200, 20));
		springLayout.putConstraint(SpringLayout.WEST, buttonRefreshColumnList, 0, SpringLayout.WEST, scrollPaneColumns);
		springLayout.putConstraint(SpringLayout.SOUTH, buttonRefreshColumnList, -16, SpringLayout.SOUTH, frame.getContentPane());
		buttonRefreshColumnList.addActionListener(buttonActionListener);
		buttonRefreshColumnList.setActionCommand(COL_REF);
		//buttonRefreshColumnList.setEnabled(false);
		frame.getContentPane().add(buttonRefreshColumnList);

		Button buttonAddColumn = new Button("+");
		springLayout.putConstraint(SpringLayout.WEST, buttonAddColumn, 8, SpringLayout.EAST, buttonRefreshColumnList);
		springLayout.putConstraint(SpringLayout.SOUTH, buttonAddColumn, -16, SpringLayout.SOUTH, frame.getContentPane());
		buttonAddColumn.addActionListener(buttonActionListener);
		buttonAddColumn.setActionCommand(COL_ADD);
		//buttonAddColumn.setEnabled(false);
		frame.getContentPane().add(buttonAddColumn);
		
		Button buttonDeleteColumn = new Button("-");
		springLayout.putConstraint(SpringLayout.WEST, buttonDeleteColumn, 8, SpringLayout.EAST, buttonAddColumn);
		springLayout.putConstraint(SpringLayout.SOUTH, buttonDeleteColumn, -16, SpringLayout.SOUTH, frame.getContentPane());
		buttonDeleteColumn.addActionListener(buttonActionListener);
		buttonDeleteColumn.setActionCommand(COL_DEL);
		//buttonDeleteColumn.setEnabled(false);
		frame.getContentPane().add(buttonDeleteColumn);		
		
		Button buttonGraph = new Button("Wykres");
		buttonGraph.setSize(new Dimension(200, 20));
		springLayout.putConstraint(SpringLayout.EAST, buttonGraph, 0, SpringLayout.EAST, scrollPaneColumns);
		springLayout.putConstraint(SpringLayout.SOUTH, buttonGraph, -16, SpringLayout.SOUTH, frame.getContentPane());
		frame.getContentPane().add(buttonGraph);
		buttonGraph.addActionListener(buttonActionListener);
		buttonGraph.setActionCommand(GRAPH);
		
		JToggleButton buttonListen = new JToggleButton("nas�uchuj");
		springLayout.putConstraint(SpringLayout.EAST, buttonListen, -8, SpringLayout.WEST, buttonGraph);
		springLayout.putConstraint(SpringLayout.VERTICAL_CENTER, buttonListen, 0, SpringLayout.VERTICAL_CENTER, buttonGraph);
		frame.getContentPane().add(buttonListen);
		buttonListen.addActionListener(buttonActionListener);
		buttonListen.setActionCommand(LISTEN);
		
		textPort = new JTextField();
		springLayout.putConstraint(SpringLayout.EAST, textPort, -4, SpringLayout.WEST, buttonListen);
		springLayout.putConstraint(SpringLayout.VERTICAL_CENTER, textPort, 0, SpringLayout.VERTICAL_CENTER, buttonGraph);
		frame.getContentPane().add(textPort);
		textPort.setColumns(4);
		textPort.setToolTipText("port");
		textPort.setText(String.valueOf(port));
		
	}
	
	private class ButtonActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			Boolean refeshDevices = false;
			Boolean refeshColumns = false;
			switch(e.getActionCommand()) {
			case DEV_ADD:
				DialogAddDevice dialogAddDevice = new DialogAddDevice();
				dialogAddDevice.setModal(true);
				dialogAddDevice.showDialog();
				refeshDevices = true;
				break;
			case DEV_DEL:
				if(listDevices.getSelectedIndex() == -1)
					return;
				try {
					DBHandler handler = DBHandler.getHandler(listDevices.getSelectedValue());
					handler.drop();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
				refeshDevices = true;
				refeshColumns = true;
				break;	
			case DEV_REF:
				refeshDevices = true;
				break;	
			case COL_ADD:
				if(listDevices.getSelectedIndex() == -1)
					return;
				DialogAddColumn dialogAddColumn = new DialogAddColumn(listDevices.getSelectedValue());
				dialogAddColumn.setModal(true);
				dialogAddColumn.showDialog();
				refeshColumns = true;
				break;	
			case COL_DEL:
				if(listDevices.getSelectedIndex() == -1 || listColumns.getSelectedIndex() == -1)
					return;
				try {
					DBHandler handler = DBHandler.getHandler(listDevices.getSelectedValue());
					String column = listColumns.getSelectedValue();
					if(column != null)
						column = column.substring(0, column.indexOf('(')-1);
					handler.dropColumn(column);
				} catch (Exception e2) {
					e2.printStackTrace();
				}
				refeshColumns = true;
				break;	
			case COL_REF:
				if(listDevices.getSelectedIndex() == -1)
					return;
				refeshColumns = true;
				break;	
			case LISTEN:
				JToggleButton button = (JToggleButton) e.getSource();
				try {
					port = Integer.parseInt(textPort.getText());
				}catch(Exception e2) {
					button.setSelected(false);
					break;
				}
				if(button.isSelected()) {
					if(port < 1024 || port > 65535) {
						button.setSelected(false);
						break;
					}
					deviceHandler = new DeviceHandler(port);
					deviceHandler.start();
				}
				else {
					deviceHandler.close();
				}
				break;
			case GRAPH:
				String device = listDevices.getSelectedValue();
				String column = listColumns.getSelectedValue();
				if(column != null)
					column = column.substring(0, column.indexOf('(')-1);
				EventQueue.invokeLater(new ChartDisplay(device, column));
				break;
			default:
				System.out.println(e.getActionCommand());
				break;
			}
			
			if(refeshDevices == true) {
				DeviceListModel deviceListModel = (DeviceListModel) listDevices.getModel();
				deviceListModel.refresh();
				listDevices.updateUI();
			}
			
			if(refeshColumns == true) {
				ColumnListModel columnListModelmodel = (ColumnListModel) listColumns.getModel();
				columnListModelmodel.refresh();
				listColumns.updateUI();
			}
		}
	}
	
	private class DeviceListSelectionListener implements ListSelectionListener{
		
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if(e.getValueIsAdjusting())
				return;
			ColumnListModel model = (ColumnListModel) listColumns.getModel();
			model.setDeviceName(listDevices.getSelectedValue());
			listColumns.updateUI();		
		}
	}
}
