package kaloryfery;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.TimeZone;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

public class ChartDisplay implements Runnable{

	private JFrame frame;
	private JCheckBox checkboxRealTime;
	private ChartPanel chartPanel;
	private JButton buttonDisplay;
	private TimeSeriesCollection timeSeriesCollection;
	private String tableName;
	private String columnName;
	
	private Thread threadRealTimeDraw;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new ChartDisplay("ayy", "lmao"));
	}
	
	public void run() {
		frame.setVisible(true);
		threadRealTimeDraw = new ThreadRealTimeDraw();
		threadRealTimeDraw.start();
	}
	
	public ChartDisplay(String table, String column) {
		this.tableName  = table;
		this.columnName = column;
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setMinimumSize(new Dimension(500, 350));
		frame.setBounds(100, 100, 704, 506);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setTitle(tableName + " - " + columnName);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if(threadRealTimeDraw != null) 					
					threadRealTimeDraw.interrupt();
			}
		});
		
		timeSeriesCollection = new TimeSeriesCollection();
		
		JFreeChart jFreeChart = ChartFactory.createTimeSeriesChart(null, null, null, timeSeriesCollection);
		
		chartPanel = new ChartPanel(jFreeChart);
		springLayout.putConstraint(SpringLayout.NORTH, chartPanel, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, chartPanel, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, chartPanel, -10, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(chartPanel);

		buttonDisplay = new JButton("wy\u015Bwietl");
		springLayout.putConstraint(SpringLayout.SOUTH, chartPanel, -10, SpringLayout.NORTH, buttonDisplay);
		springLayout.putConstraint(SpringLayout.SOUTH, buttonDisplay, -10, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, buttonDisplay, -10, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(buttonDisplay);
		buttonDisplay.addActionListener(new ButtonDisplayListener());			
		
		checkboxRealTime = new JCheckBox("Wy\u015Bwietlanie w czasie rzeczywistym");
		checkboxRealTime.setHorizontalTextPosition(SwingConstants.LEFT);
		springLayout.putConstraint(SpringLayout.NORTH, checkboxRealTime, 0, SpringLayout.NORTH, buttonDisplay);
		springLayout.putConstraint(SpringLayout.EAST, checkboxRealTime, -2, SpringLayout.WEST, buttonDisplay);
		frame.getContentPane().add(checkboxRealTime);
		checkboxRealTime.addActionListener(new ButtonDisplayListener());			
	}
	
	private class ButtonDisplayListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {		
			if(threadRealTimeDraw != null) {
				threadRealTimeDraw.interrupt();
			}
			threadRealTimeDraw = new ThreadRealTimeDraw();
			threadRealTimeDraw.start();
		}
	}
	
	class ThreadRealTimeDraw extends Thread{
		@Override
		public void run() {
			while(true) {
				drawGraph();
				if(checkboxRealTime.isSelected())
					try {
						Thread.sleep(1000*5);
					} catch (InterruptedException e) {
						return;
					}
				else
					break;
			}
		}
	}
	
	private void drawGraph() {
		DBHandler handler;
		try {
			handler = DBHandler.getHandler(tableName);
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
		if(handler == null)
			return;
		
		ArrayList<Timestamp> dates;
		ArrayList<?> values;
		
		TimeSeries timeSeries = timeSeriesCollection.getSeries(columnName);
		Boolean live = false;
		if(timeSeries == null) {
			timeSeries = new TimeSeries(String.valueOf(columnName));
			timeSeriesCollection.addSeries(timeSeries);	
			dates = new ArrayList<Timestamp>();
			values = new ArrayList<Double>();
			handler.selectColumnAsArrays(columnName, dates, values);
			live = false;
		}
		else {
			dates = new ArrayList<Timestamp>();
			values = new ArrayList<Double>();
			handler.selectColumnAsArrays(columnName, dates, values, new Timestamp(timeSeries.getNextTimePeriod().getStart().getTime()), new Timestamp((long)Integer.MAX_VALUE*1000));
			live = true;
		}
		
		switch(handler.getTypeOfColumn(columnName)) {
		
		case INTEGER: case DOUBLE:
			for(int i = 0; i<dates.size(); ++i) {
				try {
				timeSeries.add(RegularTimePeriod.createInstance(Second.class, dates.get(i), TimeZone.getDefault()), (Number) values.get(i), live);
				}catch (Exception e) {e.printStackTrace();}
			}
			break;
			
		case BOOLEAN:
			for(int i = 0; i<dates.size(); ++i) {
				try {
					int value;
					if((boolean) values.get(i))
						value = 1;
					else
						value = 0;
					timeSeries.add(RegularTimePeriod.createInstance(Second.class, dates.get(i), TimeZone.getDefault()), (Number) value, live);
				}catch (Exception e) {e.printStackTrace();}
			}
			break;
		}
		
		if(!live)
			timeSeries.fireSeriesChanged();
	}
}
