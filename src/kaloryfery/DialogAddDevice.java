package kaloryfery;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.SpringLayout;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.JTextField;

public class DialogAddDevice extends JDialog implements Runnable{

	private static final long serialVersionUID = 1L;
	JComboBox<DBHandler.Type> comboBoxTypes;
	private JTextField textFieldDeviceName;
	private JButton buttonOK;
	private JButton buttonCancel;
	//private String columnName;
	
	private static final String CANCEL = "cancel";
	private static final String OK = "OK";
	
	public static void main(String[] args) {
		try {
			EventQueue.invokeLater(new DialogAddDevice());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		setVisible(true);
	}	
	
	public void showDialog() {
		setVisible(true);
	}	

	public DialogAddDevice() {
		setBounds(100, 100, 250, 200);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		getContentPane().setLayout(springLayout);

		buttonOK = new JButton("OK");
		springLayout.putConstraint(SpringLayout.SOUTH, buttonOK, -16, SpringLayout.SOUTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, buttonOK, -16, SpringLayout.EAST, getContentPane());
		buttonOK.setEnabled(false);
		buttonOK.setActionCommand(OK);
		buttonOK.addActionListener(new ButtonActionListener());
		getContentPane().add(buttonOK);
		
		buttonCancel = new JButton("Cancel");
		springLayout.putConstraint(SpringLayout.SOUTH, buttonCancel, -16, SpringLayout.SOUTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, buttonCancel, -8, SpringLayout.WEST, buttonOK);
		buttonCancel.setActionCommand(CANCEL);
		buttonCancel.addActionListener(new ButtonActionListener());
		getContentPane().add(buttonCancel);
		
		
		textFieldDeviceName = new JTextField();
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER, textFieldDeviceName, 0, SpringLayout.HORIZONTAL_CENTER, getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, textFieldDeviceName, 16, SpringLayout.NORTH, getContentPane());
		textFieldDeviceName.setColumns(10);
		textFieldDeviceName.getDocument().addDocumentListener(new TextColumnNameListener());
		getContentPane().add(textFieldDeviceName);
	}
	
	private class ButtonActionListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			switch(e.getActionCommand()) {
			case OK:
				String tableName = textFieldDeviceName.getText().trim();
				try {
					DBHandler.createTable(tableName, null, null);
				}catch(Exception ex){
					ex.printStackTrace();
					return;
				}
				break;
			case CANCEL:
				break;
			}
			dispose();			
		}
	}

	
	private class TextColumnNameListener implements DocumentListener{

		@Override
		public void removeUpdate(DocumentEvent e) {
			if(textFieldDeviceName.getText().length() == 0) {
				buttonOK.setEnabled(false);
			}
		}
		
		@Override
		public void insertUpdate(DocumentEvent e) {
			if(textFieldDeviceName.getText().trim().length() != 0) {
				buttonOK.setEnabled(true);
			}
		}
		
		@Override
		public void changedUpdate(DocumentEvent e) {
		}
	}
}
